# Vagrant Shopware 6

A Vagrant box based containing Lubuntu linux, Apache, MySQL and PHP and Shopware 6. Provisioned with ansible.

## Requirements

* VirtualBox
* Vagrant

## Useful Info

* If this is your first time, please install the guest extension plugin:

```shell
vagrant plugin install vagrant-vbguest
```

* to start a new instance:

```shell
vagrant up
```

* from OS gui in virtual machine:

```shell
user: vagrant
pass: vagrant
```

### Software Versions

 - Lubuntu desktop 18.04
 - Apache 2.4.29
 - MySQL 5.7.25
 - Shopware 5.5.7

### Ansible Roles

 - apache
 - php
 - mysql
 - common
 - shopware

### Common Packages

 - ash-completion
 - python-pycurl
 - git
 - htop
 - tree
 - vim
 - curl
 - zip
 - rsync
 - wget
 - ant
 - tmux

### PHP Packages

- php7.2
- libapache2-mod-php7.2
- php7.2-cli
- php7.2-curl
- php7.2-gd
- php7.2-mysql
- php7.2-mbstring
- php7.2-simplexml
- php7.2-xml
- php7.2-zip
- php7.2-intl
